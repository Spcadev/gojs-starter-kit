# Alias to move to Golang development:
alias golang="cd $GOPATH"

# Export go user path, eg: GOUSRPATH="$GOPATH/src/bitbucket.com/spcadev"
GOUSRPATH="$GOPATH/src/path/to/your/user"; export GOUSRPATH
alias godev="cd $GOUSRPATH"

# Base aliases:
alias baseSrc="cd $GOUSRPATH/gojs-starter-kit/client/src"
alias baseBundle="baseSrc; jspm bundle application/main build/main-bundle.js --inject --minify"
alias baseServer="cd $GOUSRPATH/gojs-starter-kit"
alias baseRunServer="baseServer; gin -p 8000 -a 8080 -immediate"
alias baseDev="baseRunServer; baseServer"