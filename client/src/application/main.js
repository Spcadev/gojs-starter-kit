import KiRouter from 'ki-router';
import $ from 'jquery';

// Init router.
var router = KiRouter.router();

router.add('/', function (params) {
  $.get({
    url: 'http://localhost/api/index',
    success: function(response) {
      if (document.getElementById('app')) {
        console.log(response);
      }
    }
  });
});

// Set some default router settings.
router.fallbackRoute = function (url) {  };
router.hasBaseUrl = 'http://localhost/';
router.paramVerifier = function (s) { /^[a-z0-9\/]+$/i.test(s); };
router.transparentRouting();