System.config({
  baseURL: "/src/",
  defaultJSExtensions: true,
  transpiler: "babel",
  babelOptions: {
    "optional": [
      "runtime",
      "optimisation.modules.system"
    ]
  },
  paths: {
    "github:*": "jspm_packages/github/*",
    "npm:*": "jspm_packages/npm/*"
  },
  bundles: {
    "build/main-bundle.js": [
      "application/main.js",
      "npm:ki-router@1.1.17.js",
      "npm:jquery@2.2.2.js",
      "npm:ki-router@1.1.17/dist/ki-router.js",
      "npm:jquery@2.2.2/dist/jquery.js"
    ]
  },

  map: {
    "@most/multicast": "npm:@most/multicast@1.0.3",
    "babel": "npm:babel-core@5.8.35",
    "babel-runtime": "npm:babel-runtime@5.8.35",
    "core-js": "npm:core-js@1.2.6",
    "cujojs/most": "github:cujojs/most@0.18.6",
    "incremental-dom": "npm:incremental-dom@0.3.0",
    "jquery": "npm:jquery@2.2.2",
    "jsonml2idom": "npm:jsonml2idom@0.3.2",
    "ki-router": "npm:ki-router@1.1.17",
    "most": "npm:most@0.18.6",
    "github:jspm/nodelibs-assert@0.1.0": {
      "assert": "npm:assert@1.3.0"
    },
    "github:jspm/nodelibs-path@0.1.0": {
      "path-browserify": "npm:path-browserify@0.0.0"
    },
    "github:jspm/nodelibs-process@0.1.2": {
      "process": "npm:process@0.11.2"
    },
    "github:jspm/nodelibs-util@0.1.0": {
      "util": "npm:util@0.10.3"
    },
    "npm:@most/multicast@1.0.3": {
      "@most/prelude": "npm:@most/prelude@1.0.0",
      "most": "npm:most@0.18.6"
    },
    "npm:@most/prelude@1.0.0": {
      "most": "npm:most@0.18.6"
    },
    "npm:assert@1.3.0": {
      "util": "npm:util@0.10.3"
    },
    "npm:babel-runtime@5.8.35": {
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:core-js@1.2.6": {
      "fs": "github:jspm/nodelibs-fs@0.1.2",
      "path": "github:jspm/nodelibs-path@0.1.0",
      "process": "github:jspm/nodelibs-process@0.1.2",
      "systemjs-json": "github:systemjs/plugin-json@0.1.0"
    },
    "npm:incremental-dom@0.3.0": {
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:inherits@2.0.1": {
      "util": "github:jspm/nodelibs-util@0.1.0"
    },
    "npm:jsonml2idom@0.3.2": {
      "incremental-dom": "npm:incremental-dom@0.3.0"
    },
    "npm:most@0.18.6": {
      "@most/multicast": "npm:@most/multicast@1.0.3",
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:path-browserify@0.0.0": {
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:process@0.11.2": {
      "assert": "github:jspm/nodelibs-assert@0.1.0"
    },
    "npm:util@0.10.3": {
      "inherits": "npm:inherits@2.0.1",
      "process": "github:jspm/nodelibs-process@0.1.2"
    }
  }
});
