package main

import (
	"time"

	"github.com/gin-gonic/contrib/secure"
	"github.com/gin-gonic/gin"
	"github.com/itsjamie/gin-cors"
)

func main() {
	// secure middleware init
	secureMiddleware := secure.Secure(secure.Options{
		AllowedHosts:          []string{"example.com", "ssl.example.com"},
		SSLRedirect:           true,
		SSLHost:               "ssl.example.com",
		SSLProxyHeaders:       map[string]string{"X-Forwarded-Proto": "https"},
		STSSeconds:            315360000,
		STSIncludeSubdomains:  true,
		FrameDeny:             true,
		ContentTypeNosniff:    true,
		BrowserXssFilter:      true,
		ContentSecurityPolicy: "default-src 'self'; script-src 'self' 'unsafe-eval'; connect-src http://localhost:*/* ws://localhost:*/*; img-src 'self'; style-src 'self';",
		IsDevelopment:         true, // Todo: Remove this line in production!
	})

	corsMiddleware := cors.Middleware(cors.Config{
		Origins:         "*",
		Methods:         "GET, PUT, POST, DELETE",
		RequestHeaders:  "Origin, Authorization, Content-Type",
		ExposedHeaders:  "",
		MaxAge:          50 * time.Second,
		Credentials:     true,
		ValidateHeaders: false,
	})

	// initialize bare router
	router := gin.New()

	// explicitly set default middleware
	router.Use(gin.Logger())
	router.Use(gin.Recovery())

	// set security middleware
	router.Use(secureMiddleware)
	router.Use(corsMiddleware)

	// api
	router.GET("/api/index", func(c *gin.Context) {
		c.JSON(200, gin.H{"data": "Hello from index page!"})
	})

	// run server
	router.Run(":8080")
}
