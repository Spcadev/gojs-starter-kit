# STARTER: GOLANG + JAVASCRIPT #

This is a starter kit to get going with a Golang backend powered by [Gin-Gonic/Gin](https://github.com/gin-gonic/gin) and a **painless** Javascript build environment powered by [JSPM](http://jspm.io/).  This is *not intended* to be used with Go templates, and is instead supposed to be a SPA with a Golang API.

## Installation ##

**Note**: Instructions are for an OSX development environment, but will be closely matched in Linux.  I'm pretty sure if you're a dev on a Linux platform, you'll be able to figure out the Nginx install all on your own...

Clone and cd into this repo:
`git clone https://Spcadev@bitbucket.org/Spcadev/gojs-starter-kit.git`

### Directory Description ###

- **root**: (backend) Go code lives here
    - **client**: (frontend)
        - **src**: Javascript code lives here
            - **application**: *Your* javascript code lives here
            - **build**: JSPM builds the bundled, minified package in here
            - **config.js**: Used by JSPM to manage the packages.  Note: You *shouldn't* need to touch this
            - **index.js**: Inline `<script></script>` tags are a **no-no** so we use this to load the application instead
            - **package.json**: Typical node + npm goodness
        - **static**: Css, fonts and images live in here
        - **index.html**: Base starter template
    - **config**: A basic nginx config file and some bash_profile aliases for convenience
    - **server.go**: This is the **Gin** web server and entry point for the api

### Nginx (OSX) ###

This setup uses a local installation of Nginx for "load balancing" and static file serving.  This can all be done using servers built in Golang, but in a normal production environment Nginx is likely to be used in this capacity anyway, so lets get it into play for the development environment as soon as possible.

~~~
$ brew install nginx
~~~

The configuration to run this setup can be found in `/config/nginx.conf`.  If this is a clean install of nginx you can simply overwrite the existing configuration file found at `/usr/local/etc/nginx/nginx.conf` and then restart Nginx with `sudo nginx -s reload`.

| Route      | Description                                                                        |
|------------|------------------------------------------------------------------------------------|
| `/`        | This serves `index.html`, a blank `favicon.ico` file and also checks for a `maintenance.html` file, allowing us to effectively shut down the site by dropping one file into this directory.                                                                                   |
| `/api/`    | Passes API calls from the client to our Golang Gin-Gonic server.                   |
| `/src/`    | Serves gzipped Javascript.                                                         |
| `/static/` | Serves images and gzipped CSS.                                                     |

### Setup Application ###

Change into the `src` directory and get JSPM installed and ready to go:

~~~~
$ cd client/src
$ npm install
...
...
...
$ jspm install
...
...
...
$ jspm bundle application/main build/main-bundle.js --inject --minify
...
$ go get github.com/gin-gonic/gin
~~~~

### Convenience Items ###

I have the following aliases in `.bash_profile`:

~~~~
# Alias to move to Golang development:
alias golang="cd $GOPATH"

# Export go user path, eg: GOUSRPATH="$GOPATH/src/bitbucket.com/spcadev"
GOUSRPATH="$GOPATH/src/path/to/your/user"; export GOUSRPATH
alias godev="cd $GOUSRPATH"

# Base aliases:
alias baseSrc="cd $GOUSRPATH/gojs-starter-kit/client/src"
alias baseBundle="baseSrc; jspm bundle application/main build/main-bundle.js --inject --minify"
alias baseServer="cd $GOUSRPATH/gojs-starter-kit"
alias baseRunServer="baseServer; gin -p 8000 -a 8080 -immediate"
alias baseDev="baseRunServer; baseServer"
~~~~

**Note: Don't forget to restart your terminal if you modify your `.bash_profile`.**

These not only allow for some quick and easy command line action, one of them, when used in conjunction with [LiveReload](http://livereload.com/) allows us to rebundle our Javascript on save...see below for more on that.

### Filewatchers & Workflow ###

For Javascript file watching, I prefer to use [LiveReload](http://livereload.com/) coupled with the [Chrome extension](https://chrome.google.com/webstore/detail/livereload/jnihajbhpnppcggbcgedagnkighmdlei?hl=en).  After installation, open LiveReload on the Mac and point it towards the cloned repo.  There is a *"Run a custom command after processing changes"* option as well.  Ensure you check this and add *"baseBundle"*, which is the alias from above, so that the Javascript application is re-bundled after every save.

[CodeGansta/Gin](https://github.com/codegangsta/gin) is a Golang file watcher that will rebuild your Go code and restart your server on every change.  Change to the root of the repo and get that started as the final step to the installation:

~~~~
$ cd ../../
$ go get github.com/codegangsta/gin
$ gin -p 8000 -a 8080 -immediate 
~~~~

You should now be able to open your web browser to `http://localhost` and see the following in the console: `Object {data: "Hello from index page!"}`.

**Happy developing!**


